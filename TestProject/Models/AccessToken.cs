﻿using Newtonsoft.Json;

namespace TestProject.Models
{
	public class AccessToken
	{
		[JsonProperty("access_token")]
		public string Token { get; set; }
	}
}
