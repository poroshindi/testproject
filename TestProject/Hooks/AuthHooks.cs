﻿using TechTalk.SpecFlow;
using TestProject.Services;
using TestProject.Steps;

namespace TestProject.Hooks
{
	[Binding]
	public class AuthHooks : BaseSteps
	{
		public AuthHooks(ScenarioContext scenarioContext, FeatureContext featureContext)
			: base(scenarioContext, featureContext)
		{

		}

		[BeforeScenario("login")]
		public void Login()
		{
			var userLogin = "techie@email.com";
			var password = "techie";

			var service = GetService<LoginService>();
			var responseAuthLogin = service.MakeAuthLoginRequest(userLogin, password).Result;
			var authToken = responseAuthLogin.Token;

			ScenarioContext.Set(authToken, "authToken");
		}

	}
}
