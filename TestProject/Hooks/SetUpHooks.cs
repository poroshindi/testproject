﻿using System;
using System.IO;
using Allure.Commons;
using TechTalk.SpecFlow;
using TestProject.Steps;

namespace TestProject.Hooks
{
	[Binding]
	public class SetUpHooks : BaseSteps
	{
		public SetUpHooks(ScenarioContext scenarioContext, FeatureContext featureContext)
			: base(scenarioContext, featureContext)
		{
		}

		[BeforeTestRun]
		public static void SetUpTestConfiguration()
		{
			SetUpAllureConfig();
		}

		private static void SetUpAllureConfig()
		{
			string reportFolderMark = DateTime.Now.ToString("yyyyMMddHHmmss");

			Environment.SetEnvironmentVariable(
				AllureConstants.ALLURE_CONFIG_ENV_VARIABLE,
				Path.Combine(
					Environment.CurrentDirectory,
					AllureConstants.CONFIG_FILENAME));
			AllureLifecycle.Instance.CleanupResultDirectory();

			var config = AllureLifecycle.Instance.JsonConfiguration;
		}
	}
}
