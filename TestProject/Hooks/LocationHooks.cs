﻿using TechTalk.SpecFlow;
using TestProject.Models;
using TestProject.Services;
using TestProject.Steps;

namespace TestProject.Hooks
{
	[Binding]
	public class LocationHooks : BaseSteps
	{
		protected LocationHooks(ScenarioContext scenarioContext, FeatureContext featureContext)
			: base(scenarioContext, featureContext)
		{ }

		[BeforeScenario("createLocation")]
		public void CreateLocation()
		{
			var service =
				GetService<LocationsService>();
			var createdLocation = service.MakePostLocationRequest(
					locationId: 4,
					locationName: "Location0004")
				.Result;

			ScenarioContext
				.Set(createdLocation, "createdLocation");
		}

		[AfterScenario("clearLocation")]
		public void ClearLocatiion()
		{
			var service =
				GetService<LocationsService>();
			var locationToDelete =
				ScenarioContext.Get<Location>("createdLocation");

			service.MakeDeleteLocationRequest(locationToDelete.Id);
		}
	}
}
