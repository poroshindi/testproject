﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace TestProject.Steps
{
	[Binding]
	public class CommonSteps : BaseSteps
	{
		public CommonSteps(ScenarioContext scenarioContext, FeatureContext featureContext)
			: base(scenarioContext, featureContext)
		{
		}

		[Then(@"Last response codes should be equal to (.*)")]
		public void ThenResponseCodeIsEqual(string responseCodes)
		{
			var savedResponseCodes = ScenarioContext.Get<List<HttpStatusCode>>("responseCodes");
			var expectedResponseCodes = responseCodes.Split(",")
				.Select(item => item.Trim())
				.ToList();

			var skipNumber =
				Math.Max(0, savedResponseCodes.Count - expectedResponseCodes.Count);

			var actualResponseCodes =
				ScenarioContext.Get<List<HttpStatusCode>>("responseCodes")
				.Select(item => item.ToString())
				.Skip(skipNumber);

			CollectionAssert.AreEquivalent(
				expectedResponseCodes,
				actualResponseCodes,
				$"Response codes {string.Join(",", actualResponseCodes)} " +
				$"are not equal to expected {string.Join(",", expectedResponseCodes)}.");
		}
	}
}
