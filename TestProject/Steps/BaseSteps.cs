﻿using System;
using TechTalk.SpecFlow;
using TestProject.Services;

namespace TestProject.Steps
{
	[Binding]
	public abstract class BaseSteps
	{
		protected readonly ScenarioContext ScenarioContext;
		protected readonly FeatureContext FeatureContext;

		protected BaseSteps(ScenarioContext scenarioContext, FeatureContext featureContext)
		{
			ScenarioContext = scenarioContext ?? throw new ArgumentNullException(nameof(scenarioContext));
			FeatureContext = featureContext ?? throw new ArgumentNullException(nameof(featureContext));
		}

		protected T GetService<T>() where T : class, IService
		{
			var authToken = string.Empty;
			if (ScenarioContext.ContainsKey("authToken"))
				authToken = ScenarioContext.Get<string>("authToken");
			return (T)Activator.CreateInstance(typeof(T), authToken, ScenarioContext);
		}
	}
}
