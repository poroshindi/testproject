﻿using System.Collections.Generic;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TestProject.Models;
using TestProject.Services;

namespace TestProject.Steps
{
	[Binding]
	public class LocationsSteps : BaseSteps
	{
		public LocationsSteps(ScenarioContext scenarioContext, FeatureContext featureContext)
			: base(scenarioContext, featureContext)
		{
			service = GetService<LocationsService>();
		}

		private readonly LocationsService service;

		[When(@"I request for location with id (.*)")]
		public void WhenIRequestForLocationWithId(int id)
		{
			var location =
				service.MakeGetLocationRequest(id)
					.Result;
			ScenarioContext.Set(location, "location");
		}

		[When(@"I request for full location list")]
		public void WhenIRequestForFullLocationList()
		{
			var location =
				service.MakeGetAllLocationsRequest()
					.Result;
			ScenarioContext.Set(location, "fullLocationList");
		}

		[When(@"I make a request to add new location with id (.*) and name (.*)")]
		public void WhenIMakeRequestToAddNewLocationWithIdAndName(
			int id, string name)
		{
			var createdLocation = service
				.MakePostLocationRequest(
					locationId: id,
					locationName: name)
				.Result;

			ScenarioContext
				.Set(createdLocation, "createdLocation");
		}

		[When(@"I make a request to update created location for using name (.*)")]
		public void WhenIMakeRequestToUpdateCreatedLocationForUsingName(
			string name)
		{
			var createdLocation =
				ScenarioContext.Get<Location>("createdLocation");
			var updatedLocation = service
				.MakePutLocationRequest(
					locationId: createdLocation.Id,
					locationName: name)
				.Result;

			ScenarioContext
				.Set(updatedLocation, "createdLocation");
		}

		[When(@"I request for last updated location")]
		[When(@"I request for last deleted location")]
		public void WhenIRequestForLastUpdatedLocation()
		{
			var createdLocation =
				ScenarioContext.Get<Location>("createdLocation");
			var location =
				service.MakeGetLocationRequest(createdLocation.Id)
					.Result;
			ScenarioContext.Set(location, "location");
		}

		[When(@"I make a request to delete created location")]
		public void WhenIMakeRequestToDeleteCreatedLocation()
		{
			var createdLocation =
				ScenarioContext.Get<Location>("createdLocation");
			service.MakeDeleteLocationRequest(createdLocation.Id).Wait();
		}


		[Then(@"Received location id is equal to (.*), name is equal to (.*)")]
		public void ThenReceivedLocationIdIsEqualToAndNameIsEqualTo(
			int expectedId, string expectedName)
		{
			var actualLocation =
				ScenarioContext.Get<Location>("location");

			Assert.Multiple(() =>
			{
				Assert.AreEqual(expectedId, actualLocation.Id,
					"Location id is not equal to expected");
				StringAssert.AreEqualIgnoringCase(expectedName, actualLocation.Name,
					"Location name is not equal to expected.");
			});
		}

		[Then(@"Received updated location name is equal to (.*)")]
		public void ThenReceivedUpdatedLocationNameIsEqualTo(string expectedName)
		{
			var actualLocation =
				ScenarioContext.Get<Location>("location");

			StringAssert.AreEqualIgnoringCase(expectedName, actualLocation.Name,
				"Location name is not equal to expected.");
		}

		[Then(@"Location list length should be greater than zero")]
		public void ThenLocationListLenghtShoulBeEqualTo()
		{
			var actualLocationListLength =
				ScenarioContext.Get<List<Location>>("fullLocationList")
				.Count;
			Assert.Greater(actualLocationListLength, 0,
				"Location list length is not positive.");
		}

		[Then(@"Created location should has id equals to (.*) and name equals to (.*)")]
		public void ThenCreatedLocationShouldHasIdAndName(
			int expectedId, string expectedName)
		{
			var actualLocation =
				ScenarioContext.Get<Location>("createdLocation");

			Assert.Multiple(() =>
			{
				Assert.AreEqual(expectedId, actualLocation.Id,
					"Location id is not equal to expected");
				StringAssert.AreEqualIgnoringCase(expectedName, actualLocation.Name,
					"Location name is not equal to expected.");
			});
		}

		[Then(@"Updated location should has name equals to (.*)")]
		public void ThenUpdatedLocationShouldHasNameEqualsTo(string expectedName)
		{
			var actualLocation =
				ScenarioContext.Get<Location>("createdLocation");
			StringAssert.AreEqualIgnoringCase(expectedName, actualLocation.Name,
					"Location name is not equal to expected.");
		}
	}
}
