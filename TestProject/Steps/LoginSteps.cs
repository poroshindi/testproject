﻿using TechTalk.SpecFlow;
using TestProject.Services;

namespace TestProject.Steps
{
	[Binding]
	public class LoginSteps : BaseSteps
	{
		public LoginSteps(ScenarioContext scenarioContext, FeatureContext featureContext)
			: base(scenarioContext, featureContext)
		{
		}

		[When(@"I login as existing user with login (.*) and password (.*)")]
		public void WhenILoginAsExistingUserWithLoginAndPassword(string userLogin, string password)
		{

			var service = GetService<LoginService>();
			var responseAuthLogin = service.MakeAuthLoginRequest(userLogin, password).Result;
			var authToken = responseAuthLogin.Token;

			ScenarioContext.Set(authToken, "authToken");
			FeatureContext.Set(authToken, "authToken");
		}
	}
}
