﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TechTalk.SpecFlow;
using TestProject.Utils;

namespace TestProject.Services
{
	public abstract class BaseApiService : IService
	{
		private readonly string BaseUrl = "http://localhost:8880";
		private readonly string AuthToken;
		private readonly SpecFlowContext SpecFlowContext;

		public BaseApiService(string authToken, SpecFlowContext specflowContext)
		{
			AuthToken = authToken;
			SpecFlowContext = specflowContext;
		}

		protected async Task<T> MakeHttpRequest<T>(
			string url,
			HttpMethod httpMethod,
			Dictionary<string, string> headers = null,
			object body = null)
		{
			string fullUrl = $"{BaseUrl}{url}";
			using (HttpClient httpClient = new HttpClient())
			{
				HttpRequestMessage httpRequestMessage =
					new HttpRequestMessage(httpMethod, fullUrl);

				PrepareHeaders(httpRequestMessage, headers);
				PrepareBody(httpRequestMessage, body);

				var response = await httpClient.SendAsync(httpRequestMessage);
				var responseContent = await response.Content.ReadAsStringAsync();

				SetStatusCode(response.StatusCode);

				return responseContent.ConvertToObject<T>();
			}
		}

		private void SetStatusCode(HttpStatusCode statusCode)
		{
			var statusCodeList = new List<HttpStatusCode>();
			if (SpecFlowContext.ContainsKey("responseCodes"))
				statusCodeList = SpecFlowContext.Get<List<HttpStatusCode>>("responseCodes");
			statusCodeList.Add(statusCode);
			SpecFlowContext.Set(statusCodeList, "responseCodes");
		}

		private void PrepareHeaders(
			HttpRequestMessage requestMessage,
			Dictionary<string, string> headers)
		{
			if (headers == null) headers = new Dictionary<string, string>();
			if (!string.IsNullOrWhiteSpace(AuthToken))
				headers.Add("Authorization", $"Bearer {AuthToken}");

			foreach (var headerRow in headers)
				requestMessage.Headers.Add(headerRow.Key, headerRow.Value);
		}

		private void PrepareBody(
			HttpRequestMessage requestMessage,
			object body)
		{
			if (body != null)
			{
				var jsonString = JsonConvert.SerializeObject(body);
				var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
				requestMessage.Content = content;
			}
		}
	}
}
