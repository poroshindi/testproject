﻿using System.Net.Http;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TestProject.Models;

namespace TestProject.Services
{
	public class LoginService : BaseApiService, IService
	{
		public LoginService(string authToken, SpecFlowContext specflowContext)
			: base(authToken, specflowContext)
		{
		}

		public async Task<AccessToken> MakeAuthLoginRequest(string userLogin, string userPassword)
		{
			var token = await MakeHttpRequest<AccessToken>(
				url: "/auth/login",
				httpMethod: HttpMethod.Post,
				body: new
				{
					email = userLogin,
					password = userPassword
				});

			return token;
		}
	}
}
