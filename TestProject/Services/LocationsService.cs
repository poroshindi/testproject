﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TestProject.Models;

namespace TestProject.Services
{
	public class LocationsService : BaseApiService, IService
	{
		public LocationsService(string authToken, SpecFlowContext specflowContext)
			: base(authToken, specflowContext)
		{
		}

		public async Task<Location> MakeGetLocationRequest(int locationId)
		{
			string url = $"/locations/{locationId}";
			var location = await MakeHttpRequest<Location>(
				url,
				HttpMethod.Get);

			return location;
		}

		public async Task<List<Location>> MakeGetAllLocationsRequest()
		{
			string url = $"/locations";
			var location = await MakeHttpRequest<List<Location>>(
				url,
				HttpMethod.Get);

			return location;
		}

		public async Task<Location> MakePostLocationRequest(
			int locationId, string locationName)
		{
			string url = $"/locations";
			var locationToAdd = new Location()
			{
				Id = locationId,
				Name = locationName
			};

			var createdLocationResponse = await MakeHttpRequest<Location>(
				url: url,
				httpMethod: HttpMethod.Post,
				body: locationToAdd);

			return createdLocationResponse;
		}

		public async Task<Location> MakePutLocationRequest(
			int locationId, string locationName)
		{
			string url = $"/locations/{locationId}";
			var locationToUpdate = new Location()
			{
				Id = locationId,
				Name = locationName
			};

			var updatedLocationResponse = await MakeHttpRequest<Location>(
				url: url,
				httpMethod: HttpMethod.Put,
				body: locationToUpdate);

			return updatedLocationResponse;
		}

		public async Task MakeDeleteLocationRequest(int locationId)
		{
			string url = $"/locations/{locationId}";
			await MakeHttpRequest<Location>(
				url,
				HttpMethod.Delete);
		}
	}
}
