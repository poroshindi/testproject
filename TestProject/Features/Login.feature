﻿Feature: Login User

Scenario Outline: Login with user name and password
	When I login as existing user with login <userLogin> and password <password>
	Then Last response codes should be equal to <responseCode>

Examples: 
| userLogin            | password | responseCode |
| techie@email.com     | techie   | OK           |
| notexisting@emai.com | pswd     | Unauthorized |