﻿@login
Feature: Locations API endpoint

Scenario Outline: Get location by id
	When I request for location with id <locationId>
	Then Last response codes should be equal to OK
	And Received location id is equal to <locationId>, name is equal to <expectedName>

Examples: 
| locationId | expectedName |
| 1          | Location001  |
| 2          | Location002  |
| 3          | Location003  |

Scenario: Get full location list
	When I request for full location list
	Then Last response codes should be equal to OK
	And Location list length should be greater than zero

@clearLocation
Scenario: Add new location
	When I make a request to add new location with id 4 and name Location004
	And I request for location with id 4
	Then Last response codes should be equal to Created,OK
	And Created location should has id equals to 4 and name equals to Location004
	And Received location id is equal to 4, name is equal to Location004

@createLocation @clearLocation
Scenario: Update location name
	When I make a request to update created location for using name Location005
	And I request for last updated location
	Then Last response codes should be equal to OK,OK
	And Updated location should has name equals to Location005
	And Received updated location name is equal to Location005

@createLocation	
Scenario: Delete existing location
	When I make a request to delete created location
	And I request for last deleted location
	Then Last response codes should be equal to OK,NotFound