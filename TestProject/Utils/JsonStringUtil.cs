﻿using Newtonsoft.Json;

namespace TestProject.Utils
{
	public static class JsonStringUtil
	{
		public static T ConvertToObject<T>(this string jsonString) =>
			JsonConvert.DeserializeObject<T>(jsonString);
	}
}
